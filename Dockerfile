# FROM ubuntu:14.04
FROM python:3

RUN apt-get update && apt-get install -y gcc g++ gfortran perl bash curl cmake

RUN pip install pyparsing

WORKDIR /opt/molcas/src

ADD . /opt/molcas/src

WORKDIR /opt/molcas/build

RUN cmake ../src

RUN make

# Run undefinetly, use bash to enter in
ENTRYPOINT [ "tail", "-f", "/dev/null" ]
# ENTRYPOINT [ "bash" ]